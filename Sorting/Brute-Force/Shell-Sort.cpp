#include<iostream>

using namespace std;

int shellSort(int a[], int n)
{
    for(int gap = n/2; gap > 0; gap /=2)
    {
        for(int i = gap; i < n; i += 1)
        {
            int temp = a[i];
            int j = 0;
            for(j = i; j >= gap && a[j - gap] > temp; j -= gap)
                a[j] = a[j - gap];

            a[j] = temp;
        }
    }

    return 0;
}

int main()
{
    int arr[] = { 12, 11, 13, 5, 6 };
    int n = sizeof(arr) / sizeof(arr[0]);

    shellSort(arr, n);

    return 0;
}
