#include<iostream>

using namespace std;

void selectionSort(int a[], int n)
{
    for(int i = 0; i < n; i++)
    {
        int max = a[i];
        for(int j = i; j < n; j++)
        {
            if(a[j] < a [j+1])
              max = a[j];
        }
        int temp = a[i];
        a[i] = a[max];
        a[max] = temp;
    }
}


int main()
{
    int arr[] = { 12, 11, 13, 5, 6 };
    int n = sizeof(arr) / sizeof(arr[0]);

    selectionSort(arr, n);

    return 0;
}
