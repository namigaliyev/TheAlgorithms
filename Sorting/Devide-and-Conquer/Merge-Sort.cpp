#include<iostream>

using namespace std;

void merge(int a[], int left, int m, int right)
{
    int i, j, k;
    int n1 = m - left + 1;
    int n2 = right - m;

    int L[n1], R[n2];

    for(i = 0; i < n1; i++)
    {
        L[i] = a[left + i];
    }
    for(j = 0; j < n2; j++)
    {
        R[j] = a[m + 1 + j];
    }

    i = 0;
    j = 0;
    k = left;

    while(i < n1 && j < n2)
    {
        if(L[i] < = R[j])
        {
            a[k] = L[i];
            i++;
        }
        else
        {
            a[k] = R[j];
            j++;
        }
        k++;
    }

    while(i < n1)
    {
        a[k] = L[i];
        i++;
        k++;
    }

    while(j < n2)
    {
        a[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int a[], int left, int right)
{
    if(left < right)
    {
        int m = left + (right - left)/ 2;

        mergeSort(a, left, m);
        mergeSort(a, m + 1, right);

        merge(a, left, m, right);
    }
}


int main()
{
    int arr[] = {38, 27, 43, 3, 9, 82, 10};
    int arr_size = sizeof(arr)/sizeof(arr[0]);

    mergeSort(arr, 0, arr_size - 1);

    return 0;
}
