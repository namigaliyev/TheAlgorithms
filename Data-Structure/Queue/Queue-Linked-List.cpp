#include<iostream>

using namespace std;

template <typename Object>
class Node
{
  public:
    Object item;
    Node<Object> *next;
    Node<Object itm, Node<Object> *nxt = NULL)
    {
        item = itm;
        next = nxt;
    }
};

template <typenamee Object>
class Queue
{
  private:
    Node<Object> *front;
    Node<Object> *back;
    int length;

  public:
    Queue()
    {
        front = back = NULL;
        length = 0;
    }

    void clear()
    {
        while(!isEmpty())
            dequeue();
    }

    bool isEmpty() const
    {
        return length == 0 ? true : false;
    }

    const Object& peek() const
    {
        //if(isEmpty()) throw queueEmpty
        return front->item;
    }

    void enqueue(const Object& item)
    {
        Node<Object> *last = new Node<Object>(item);
        if(isEmpty())
            front = back = last;
        else
        {
            back->next = last;
            back = last;
        }
        length++;
    }

    void dequeue()
    {
        //if(isEmpty()) throw queueEmpty
        Node<Object> *temp = front;
        front = front->next;
        delete temp;
        length--;
    }
    ~Queue()
    {
        clear();
    }
};

struct Student
{
    int no;
    string nameSurname;
    Student(int number, string name)
    {
        no = number;
        nameSurname = name;
    }

    friend ostream& operator<<(ostream& screen, Student& right)
    {
        screen << right.no << " " << right.nameSurname << endl;
        return screen;
    }
};

int main()
{
    Queue<Student*> *queue = new Queue<Student*>();
    Student *david = new Student(224, "David Fowler");
    Student *malan = new Student(100, "David J.Malan");
    Student *durant = new Student(500, "Kevin Durant");

    queue->enqueue(david);
    queue->enqueue(malan);
    cout << *(queue->peek()) << endl;
    queue->dequeue();
    cout << *(queue->peek()) << endl;
    queue->enqueue(durant);
    queue->enqueue(david);
    cout << *(queue->peek()) << endl;
    delete david;
    delete malan;
    delete durant;
    delete queue;
    return 0;
}
