#include<iostream>

using namespace std;

template <typename Object>
class Node
{
  public:
    Object item;
    Node<Object> *next;
    Node<Object itm, Node<Object> *nxt = NULL)
    {
        item = itm;
        next = nxt;
    }
};

template <typenamee Object>
class Queue
{
  private:
    Node<Object> *front;
    Node<Object> *back;
  public:
    Queue()
    {
        front = NULL;
        rear = NULL;
    }

    void createNode(int value)
    {
        Node<Object> *temp = new Node<Object>();
        temp->item = value;
        temp->next = NULL;
        front = temp;
        rear = temp;
    }

    void enqueue(int value)
    {
        if(front == NULL || rear == NULL)
        {
            createNode(value);
        }
        else
        {
            Node<Object> *temp = new Node<Object>();
            temp->data = value;
            rear->next = temp;
            temp->next = front;
            rear = temp;
        }
    }

    void dequeue()
    {
        Node<Object> *temp;
        temp = front;
        front = front->next;
        delete(temp);
    }

    void traverse()
    {
        Node<Object> *temp;
        temp = front;
        do{
          cout << ptr->data << " ";
        }while(ptr != rear->next);
        cout << front->data;
        cout << endl;
    }
};

struct Student
{
    int no;
    string nameSurname;
    Student(int number, string name)
    {
        no = number;
        nameSurname = name;
    }

    friend ostream& operator<<(ostream& screen, Student& right)
    {
        screen << right.no << " " << right.nameSurname << endl;
        return screen;
    }
};

int main()
{
    Queue<Student*> *queue = new Queue<Student*>();
    Student *david = new Student(224, "David Fowler");
    Student *malan = new Student(100, "David J.Malan");
    Student *durant = new Student(500, "Kevin Durant");

    queue->enqueue(david);
    queue->enqueue(malan);
    queue->dequeue();
    queue->traverse();
    queue->enqueue(durant);
    queue->enqueue(david);
    queue->traverse();
    delete david;
    delete malan;
    delete durant;
    delete queue;
    return 0;
}
