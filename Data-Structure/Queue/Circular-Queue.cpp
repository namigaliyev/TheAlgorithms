#include<iostream>
#include<cmath>

using namespace std;

template <typename Object>
class Queue
{
  private:
    int front;
    int rear;
    int capacity;
    int length;
    Object *queue;

    void reverse(int newCapacity)
    {
        Object *temp = new Object[newCapacity];
        for(int i=front, j=0; j<length; j++, i++)
        {
            temp[j] = items[i];
            i = (i + 1) % capacity;
        }
        capacity = newCapacity;
        delete[] items;
        items = temp;
        front = 0;
        rear = length - 1;
    }

  public:
    Queue()
    {
        capacity = 1;
        front = 0;
        rear = -1;
        length = 0;
        queue = new Object[capacity];
    }

    void clear()
    {
        front = 0;
        rear = -1;
        length = 0;
    }

    int count() const
    {
        return length;
    }

    bool isEmpty() const
    {
        return length == 0 ? true : false;
    }

    const Object& peek() const
    {
        //if(isEmpty()) throw queueEmpty
        return queue[front];
    }

    void enqueue(const Object& item)
    {
        if(length == capacity)
            reverse(2*capacity);
        rear = (rear + 1) % capacity;
        queue[rear] = item;
        length++;
    }

    void dequeue()
    {
        //if(isEmpty()) throw queueEmpty
        front = (front + 1) % capacity;
        length--;
    }
    ~Queue()
    {
        delete[] items;
    }
};

struct Student
{
    int no;
    string nameSurname;
    Student(int number, string name)
    {
        no = number;
        nameSurname = name;
    }

    friend ostream& operator<<(ostream& screen, Student& right)
    {
        screen << right.no << " " << right.nameSurname << endl;
        return screen;
    }
};

int main()
{
    Queue<Student*> *queue = new Queue<Student*>();
    Student *david = new Student(224, "David Fowler");
    Student *malan = new Student(100, "David J.Malan");
    Student *durant = new Student(500, "Kevin Durant");

    queue->enqueue(david);
    queue->enqueue(malan);
    cout << *(queue->peek()) << endl;
    queue->dequeue();
    cout << *(queue->peek()) << endl;
    queue->enqueue(durant);
    queue->enqueue(david);
    cout << *(queue->peek()) << endl;
    delete david;
    delete malan;
    delete durant;
    delete queue;
    return 0;
}
