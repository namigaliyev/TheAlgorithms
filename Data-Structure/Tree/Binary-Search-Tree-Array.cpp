#include<iostream>
#include<cmath>

using namespace std;
define SIZE 1000

template <typename Object>
class BST
{
  private:
    Object *elements;
    int indexOccupancy[SIZE];

    int Search(const Object& element, int currentIndex = 0)
    {
        while(true)
        {
            if(currentIndex == SIZE || indexOccupancy[currentIndex] == 0) return -1;
            if(elements[currentIndex] == element && indexOccupancy[currentIndex] == 1) {
                return currentIndex;
            }
            else if(element < elements[currentIndex]) currentIndex = 2*currentIndex + 1;
            else if(element > elements[currentIndex]) currentIndex = 2*currentIndex + 2;
        }
    }

    bool isDouble(int x)
    {
        while(((x % 2) == 0) && x > 1)
            x /= 2;
        return (x == 1);
    }

    int smallestValue(int i)
    {
        if(2*i+1 > SIZE) return i;
        else if(indexOccupancy[2*i+1] == 0) return i;
        else return smallestValue(2*i+1);
    }
  public:
    BST()
    {
        elements = new Object[SIZE];
        clear();
    }

    void add(const Object& element)
    {
        int currentIndex = 0;
        while(true)
        {
            if(indexOccupancy[currentIndex] == 0)
            {
                elements[currentIndex] = element;
                indexOccupancy[currentIndex] = 1;
                break;
            }
            else if(element < elements[currentIndex]) currentIndex = 2*currentIndex + 1;
            else if(element > elements[currentIndex]) currentIndex = 2*currentIndex + 2;
            else return; //this tree have some element and don't add element
        }
    }

    bool isAvailable(const Object *element)
    {
        if(Search(element) == -1) return false;
        return true;
    }

    int height(int index = 0)
    {
        if(indexOccupancy[index] == 0) return -1;
        return 1 + max(height(2*index + 1), height(2*index + 2));
    }

    void delete(const Object& element, int ind = 0)
    {
        int i = Search(elemet, ind);
        if(i == -1) return; //throw elementNotFound()
        else
        {
            indexOccupancy[i] = 0;
            if(indexOccupancy[2*i + 2] == 0)
            {
                if(indexOccupancy[2*i +1] == 1) //only left child
                {
                    int k = 2*i+1; int temp[SIZE];
                    for(int iter = 0; iter < SIZE; iter++) temp[iter] = 0;
                    temp[0] = k; int index = 0;

                    //BFS
                    for(int iter = 0; temp[iter] !=0 && iter < SIZE; iter++)
                    {
                        for(int x = 0; x < SIZE; x++)
                        {
                            if(isDouble(temp[iter] + 1 -x))
                            {
                                elements[(temp[iter] + x - 1)/2] = elements[temp[iter]];
                                indexOccupancy[(temp[iter] + x - 1)/2] = 1;
                                indexOccupancy[temp[iter]] = 0;
                                break;
                            }
                            if(indexOccupancy[2*temp[iter] + 1] != 0)
                            {
                                temp[++index] = 2*temp[iter] + 1;
                            }
                            if(indexOccupancy[2*temp[iter] + 2] != 0)
                            {
                                temp[++index] = 2*temp[iter] + 2;
                            }
                        }
                    }
                }
                else
                {
                    if(indexOccupancy[2*i + 1] == 0) //only right child
                    {
                        int k = 2*i+2; int temp[SIZE];
                        for(int iter = 0; iter < SIZE; iter++) temp[iter] = 0;
                        temp[0] = k; int index = 0;

                        //BFS
                        for(int iter = 0; temp[iter] != 0 && iter < SIZE; iter++)
                        {
                            for(int x = 0; x < SIZE; x++)
                            {
                                if(idDouble((temp[iter] + x)/2 + 1))
                                {
                                    eements[(temp[iter] - 2 - x)/2] = elements[temp[iter]];
                                    indexOccupancy[(temp[iter] - 2 - x)/2] = 1;
                                    break;
                                }
                            }
                            indexOccupancy[temp[iter]] = 0;
                            if(indexOccupancy[2*temp[iter] + 1] != 0)
                            {
                                temp[++index] = 2*temp[iter] + 1;
                            }
                            if(indexOccupancy[2*temp[iter] +2] != 0)
                            {
                                temp[++index] = 2*temp[iter] + 2;
                            }
                        }
                    }
                    else
                    {
                        int x = smallestValue(2*i+2);
                        Object temp2 = elements[x];
                        indexOccupancy[i] = indexOccupancy[x];
                        elements[i] = elements[x];
                        delete(temp2, x);
                    }
                }
            }
        }
    }

    void inorder(int currentIndex = 0)
    {
        if(indexOccupancy[currentIndex] != 0 && currentIndex < SIZE)
        {
            Inorder(2*currentIndex+1);
            cout << elements[currentIndex] << " ";
            Inorder(2*currentIndex+2);
        }
    }

    void Preorder(int currentIndex = 0)
    {
        if(indexOccupancy[currentIndex] != 0 && currentIndex < SIZE)
        {
            cout << elements[currentIndex] << " ";
            Preorder(2*currentIndex+1);
            Preorder(2*currentIndex+2);
        }
    }

    void Postorder(int currentIndex = 0)
    {
        if(indexOccupancy[currentIndex] != 0 && currentIndex < SIZE)
        {
            Postorder(2*currentIndex+1);
            Postorder(2*currentIndex+2);
            cout << elements[currentIndex] << " ";
        }
    }

    void clear()
    {
        for(int i=0; i<SIZE; i++) indexOccupancy[i] = 0;
    }

    ~BST()
    {
        delete [] elements;
    }
};

int main()
{
    BST<int> *tree = new BST<int>();
    tree->add(15);
    tree->add(1);
    tree->add(157);
    tree->add(215);
    tree->add(315);
    tree->add(160);
    tree->add(33);
    tree->add(200);
    tree->add(220);
    tree->add(80);
    tree->add(300);
    tree->add(290);
    cout << "Inorder :";
    tree->Inorder();
    cout << endl << "Preorder :";
    tree->Preorder();
    cout<<endl<<"Postorder :";
    tree->Postorder();
    cout << endl << endl;
    tree->delete(15);
    cout<<"after basic node deleted:"<<endl;
    cout << "Inorder :";
    tree->Inorder();
    cout << endl << "Preorder :";
    tree->Preorder();
    cout<<endl<<"Postorder :";
    tree->Postorder();
    cout << endl << endl;
    cout<<"Yukseklik:"<<agac->height()<<endl;
  	tree->Inorder(); cout<<endl;
  	tree->Preorder(); cout<<endl;
  	tree->Postorder(); cout<<endl;

    delete tree;
    return 0;
}
