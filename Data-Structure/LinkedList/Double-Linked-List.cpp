#include<iostream>

using namespace std;

struct Node
{
    int data;
    Node *next;
    Node *prev;
};
class LinkedList
{
  private:
    Node *head, *tail;

  public:
      LinkedList()
      {
          head = NULL;
          tail = NULL;
      }

      void insertStart(int value)
      {
          if(head == NULL)
          {
              Node *temp = new Node;
              temp->data = value;
              temp->next = NULL;
              temp->prev = NULL;
          }
          else
          {
              Node *temp = new Node;
              temp->data = data;
              temp->next = head;
              temp->prev = NULL;
              head = temp;
          }
      }

      void insertPosition(int position)
      {
          Node *previous = new Node; //onceki dugum
          Node *current = new Node; //sonraki dugum
          Node *temp = new Node; //eklenecek dugum
          current = head;
          for(int i=1; i< position; i++)
          {
              previous = current;
              current = current->next;
          }
          temp->data = value;
          previous->next = temp;
          temp->next = current;
          temp->prev = current->prev;
          current->prev = temp;
      }

      void insertEnd()
      {
          if(head == NULL)
          {
              Node *temp = new Node;
              temp->data = data;
              temp->next = NULL;
              temp->prev = NULL;
              head = temp;
          }
          else
          {
              Node *previous = new Node; //onceki node
              Node *temp = new Node; //eklenecke node
              previous = head;
              while(previous->next != NULL)
              {
                  previous = previous->next;
              }

              temp->data = data;
              temp->next = NULL;
              temp->prev = previous;
              previous->next = temp;
          }
      }

      void deleteStart()
      {
          Node *temp;
          temp = head;
          head = head->next;
          free(temp);
          head->prev = NULL;
      }

      void deletePosition(int position)
      {
          Node *previous = new Node; //onceki
          Node *current = new Node; //sonraki
          Node *temp = new Node;  //silinecek  node
          temp = head;
          for(int i=1; i < position; i++)
          {
              previous = temp;
              temp = temp->next;
          }
          current = temp->next;
          previous->next = current;
          current->prev = previous;
          free(current);
      }

      void deleteEnd()
      {
          Node *temp2 = new Node;
          Node *temp = new Node; //silinecek node
          temp = head;
          while(temp->next != NULL)
          {
              temp = temp->next;
          }
          temp2 = temp->prev;
          free(temp);
          temp2->next = NULL;
      }

      void reverse()
      {
          Node *temp2 = new Node;
          Node *temp1 = NULL;
          Node *temp = NULL;
          while(temp2)
          {
              temp1 = temp2->next;
              temp2->next = temp;
              temp2->prev = temp1;
              temp = temp2;
              temp2 = temp1;
          }

          head =  temp;
      }

      void display()
      {
          Node *temp = new Node;
          temp = head;
          while(temp != NULL)
          {
              cout << temp->data << "<->";
              temp = temp->next;
          }
      }
};

int main()
{
     system("cls");
     cout << "\n\t\t DOUBLY Linked List\n";
     int x;
     cout << "Select the operation: \n";
     cout << "1. Insert @beginning\n2. Insert @end\n3. Insert @index\n4. Delete @beginning\n5. Delete @end\n6. Delete @index\n7. Reverse\n8. Print the list\n";
     cout << "Your choice: ";
     cin >> x;
     switch (x)
     {
     case 1:
         insert_beg_d();
         break;
     case 2:
         insert_end_d();
         break;
     case 3:
         insert_ind_d();
         break;
     case 4:
         del_beg_d();
         break;
     case 5:
         del_end_d();
         break;
     case 6:
         del_ind_d();
         break;
     case 7:
         rev_d();
         break;
     case 8:
         printi_d();
         break;
     default:
         cout << "\nInvalid choice";
         break;
     }

    return 0;
}
