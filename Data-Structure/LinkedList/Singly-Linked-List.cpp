#include<iostream>

using namespace std;

struct Node
{
    int data;
    Node *next;
};

class LinkedList
{
  private:
    Node *head, *tail;

  public:
      LinkedList()
      {
          head = NULL;
          tail = NULL;
      }
      //Add node list end
      void createNode(int value)
      {
          Node *temp = new Node;
          temp->data = value;
          temp->next = NULL;
          if(head == NULL)
          {
              head = temp;
              tail = temp;
              temp = NULL;
          }
          else
          {
              tail->next = temp;
              tail = temp;
          }
      }

      void insertStart(int value)
      {
          Node *temp = new Node;
          temp->data = value;
          temp->next = head;
          head = temp;
      }

      void insertPosition(int position, int value)
      {
          Node *previous = new Node;       //onceki dugum
          Node *current = new Node;       //sonraki dugum
          Node *temp = new Node;      //eklenecek dugum
          current = head;
          for(int i=1; i<position; i++)
          {
              previous = current;
              current = current->next;
          }
          temp->data = value;
          previous->next = temp;
          temp->next = current;
      }

      void deleteFirst()
      {
          Node *temp = new Node;
          temp = head;
          head = head->next;
          delete temp;
      }

      void deleteLast()
      {
          Node *current = new Node;
          Node *previous = new Node;
          current = head;
          while(current->next != NULL)
          {
              previous = current;
              current = current->next;
          }
          tail=previous;
          previous->next = NULL;
          delete current;
      }

      void deletePosition(int position)
      {
          Node *current = new Node;
          Node *previous = new Node;
          current = head;
          for(int i = 0; i<position; i++)
          {
              previous = current;
              current = current->next;
          }
          previous->next = current->next;
      }

      void search(int key)
      {
          int position = 1;
          bool found = false;
          Node *temp = new Node;
          temp = head;
          while(temp! = NULL)
          {
              if(temp->data == key)
              {
                  cout << "\nFound at position:" << " " << position;
                  found = true;
                  break;
              }
              temp = temp->next;
              position++;
          }
          if(found!=true){cout<<"\nElement doesn't exist in LinkedList";}
      }

      void display()
      {
          Node *temp = new Node;
          temp = head;
          while(temp != NULL)
          {
              cout<<temp->data<<"\t";
              temp = temp->next;
          }
      }
};

int main()
{
    LinkedList object;
    object.createNode(25);
    object.createNode(50);
    object.createNode(90);
    object.createNode(40);
    cout<<"---------------Displaying All nodes---------------";
    object.display();
    cout<<"-----------------Inserting At End-----------------";
    object.createNode(55);
    object.display();
    cout<<"----------------Inserting At Start----------------";
    object.insertStart(44);
    object.display();
    cout<<"-------------Inserting At Particular--------------";
    object.insertPosition(5,60);
    object.display();
    cout<<"----------------Deleting At Start-----------------";
    object.deleteFirst();
    object.display();
    cout<<"-----------------Deleting At End-------------------";
    object.deleteLast();
    object.display();
    cout<<"--------------Deleting At Particular--------------";
    object.deletePosition(4);
    object.display();
    cout<<"--------------Searching for an element--------------";
	  cout<<"\n--------------------------------------------------\n";
  	obj.search(50);

    system("pause");
    return 0;
}
