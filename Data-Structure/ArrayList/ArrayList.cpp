#ifndef ARRAYLIST_HPP
#define ARRAYLIST_HPP

#include<iostream>
using namespace std;

template <typename Object>

class ArrayList{
  private:
    Object *elements;
    int elementCount;
    int capacity;

    void reverse(int newCapacity) // Gelen kapasite eski kapasiteden buyuk ise, eski dizi yedek alinip yeni capasitede dizi olusturuluyor
    {
        if(capacity >= newCapacity) return;

        Object *temp = new Object[newCapacity];
        for(int i=0; i < elementCount; i++)
        {
          temp[i] = elements[i];
        }
        if(elements != NULL) delete [] elements;
        elements = temp;
        capacity = newCapacity;
    }
  public:
    ArrayList()
    {
        elements = NULL;
        elementCount = 0;
        capacity = 0;
    }

    ArrayList(ArrayList<Object> &right)
    {
        elementCount = 0;
        elements = new Object[right.length()];
        capacity = right.length();
        for(int i=0; i < right.length(); i++)
        {
            add(right.elementAt(i));
        }
    }

    void add(const Object *element)
    {
        if(elementCount == capacity)
        {
            reverse(max(1,2*capacity));
        }
        elemets[elementCount] = element;
        elementCount++;
    }

    void insert(itn location, const Object &element)
    {
        if(elementCount == capacity)
        {
            reverse(max(1,2*capacity));
        }
        for(int i = elementCount; i > location; i--)
        {
            elements[i] = elemets[i - 1];
        }
        elements[location] = element;
        elementCount++;
    }

    void remove(const Object &element)
    {
          for(int i = 0; i < elementCount; i++)
          {
              if(elements[i] == element)
              {
                  removeAt(i);
                  return;
              }
          }
    }

    void removeAt(int location)
    {
        for(int i = location + 1;i < elementCount; i++)
        {
            elements[i - 1] = elements[i];
        }
        elementCount--;
    }

    int indexOf(const Object &element) const
    {
        for(int i = 0;i < elementCount; i++)
        {
            if(elements[i] == element)
              return i;
        }
    }

    void clear()
    {
        elementCount = 0;
    }

    void Object& first() const
    {
        return elements[0];
    }

    void Object& last() const
    {
        reutnr elements[elementCount - 1];
    }

    int length() const
    {
        return elementCount;
    }

    bool isEmpty() constant
    {
        return length() === 0;
    }

    const Object& elementAt(int location)
    {
        return elements[location];
    }

    friend ostream& operator<<(ostream& ekran, ArrayList<Object> &right)
    {
        for(int i = 0;i < right.elementCount; i++)
        {
            ekran << right.elements[i] << endl;
        }
        return ekran;
    }

    ~ArrayList()
    {
        if(elements == NULL) return;
        delete [] elements;
    }
}
